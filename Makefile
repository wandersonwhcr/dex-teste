COMPOSER_FLAGS = --prefer-dist

all: install

.PHONY: install
install:
	composer install ${COMPOSER_FLAGS}
	bower install
	php artisan migrate

.PHONY: optimize
optimize: COMPOSER_FLAGS = --no-dev --optimize-autoloader --classmap-authoritative
optimize: install
