<?php

namespace App\Http\Controllers;

use App\Store;
use File;
use Illuminate\Http\Request;
use Response;

/**
 * Controladora de Lojas
 */
class Stores extends Controller
{
    /**
     * Lista as Lojas Abertas
     */
    public function list()
    {
        // Hora Atual
        $currentTime = date('H:i', strtotime('now'));

        // Consulta
        $stores = Store
            ::where('openingHoursBegin', '<=', $currentTime)
            ->where('openingHoursEnd', '>=', $currentTime)
            ->get();

        // Apresentação
        return view('stores', [
            'stores' => $stores,
        ]);
    }

    /**
     * Apresenta o Logotipo da Loja
     *
     * @param int $id Chave Primária do Elemento
     */
    public function logo($id)
    {
        // Inicialização
        $filepath = storage_path('app/public/store-logo-' . $id);

        // Existente?
        if (! File::exists($filepath)) {
            // Impossível Continar
            return abort(404);
        }

        // Captura
        $file = File::get($filepath);
        $type = File::mimeType($filepath);

        // Inicialização
        $response = Response::make($file, 200);
        $response->header('Content-Type', $type);

        // Apresentação
        return $response;
    }
}
