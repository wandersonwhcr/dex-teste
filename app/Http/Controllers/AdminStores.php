<?php

namespace App\Http\Controllers;

use App\Store;
use DateTime;
use File;
use Illuminate\Http\Request;
use Validator;

/**
 * Controladora de Lojas no Painel Administrativo
 */
class AdminStores extends Controller
{
    /**
     * Listagem de Lojas
     */
    public function list()
    {
        // Consulta
        $stores = Store::orderBy('created_at', 'ASC')->get();

        // Apresentação
        return view('admin/stores/list', [
            'stores' => $stores,
        ]);
    }

    /**
     * Cria uma Nova Loja
     *
     * @param int|null $id Chave Primária do Elemento
     */
    public function form($id = null)
    {
        // Configurado?
        if (isset($id)) {
            // Consulta
            $store = Store::findOrFail($id);
        } else {
            // Nova Loja
            $store = new Store();
        }

        // Apresentação
        return view('admin/stores/form', [
            'store' => $store,
        ]);
    }

    /**
     * Remove uma Loja
     *
     * @param int $id Chave Primária do Elemento
     */
    public function remove($id)
    {
        // Consulta
        $store = Store::findOrFail($id);

        // Apresentação
        return view('admin/stores/delete', [
            'store' => $store,
        ]);
    }

    /**
     * Cria uma Nova Loja
     */
    public function post(Request $request, $id = null)
    {
        // Validador de Horário de Funcionamento
        Validator::extend('openingHours', function ($attribute, $value, $parameters, $validator) {
            // Inicializar Capturas
            list($bHours, $bMinutes) = explode(':', $value);
            list($eHours, $eMinutes) = explode(':', $validator->getData()['store']['openingHoursEnd']);
            // Hora Inicial
            $openingHoursBegin = new DateTime();
            $openingHoursBegin->setDate(0, 0, 0);
            $openingHoursBegin->setTime($bHours, $bMinutes);
            // Hora Final
            $openingHoursEnd = new DateTime();
            $openingHoursEnd->setDate(0, 0, 0);
            $openingHoursEnd->setTime($eHours, $eMinutes);
            // Verifica Horário de Abertura Anterior ao Horário de Fechamento
            return $openingHoursBegin->diff($openingHoursEnd)->invert === 0;
        });

        // Validador
        $validator = Validator::make($request->all(), [
            'store.name'              => 'required|max:255',
            'store.description'       => 'required',
            'store.openingHoursBegin' => 'required|date_format:H:i|openingHours',
            'store.openingHoursEnd'   => 'required|date_format:H:i',
            'store.logo'              => (! isset($id) ? 'required|image' : 'sometimes|required|image'),
        ]);

        // Dados Válidos?
        if ($validator->fails()) {
            // Chave Primária?
            if (! isset($id)) {
                // Apresentar Erros
                return redirect()
                    ->route('admin/stores/create')
                    ->withInput()
                    ->withErrors($validator);
            } else {
                // Apresentar Erros
                return redirect()
                    ->route('admin/stores/update', ['id' => $id])
                    ->withInput()
                    ->withErrors($validator);
            }
        }

        // Chave Primária?
        if (! isset($id)) {
            // Criar Loja
            $store = new Store();
        } else {
            // Consultar Loja
            $store = Store::findOrFail($id);
        }

        // Configurações
        $store->name              = $request->store['name'];
        $store->description       = $request->store['description'];
        $store->openingHoursBegin = $request->store['openingHoursBegin'];
        $store->openingHoursEnd   = $request->store['openingHoursEnd'];

        // Salvar Informações
        $store->save();

        // Logotipo Enviado?
        if (isset($request->store['logo'])) {
            // Logotipo
            $file = $request->store['logo'];
            // Movimentar ao Diretório Correto
            $file->move(base_path('storage/app/public'), 'store-logo-' . $store->id);
        }

        // Chave Primária?
        if (! isset($id)) {
            // Redirecionamento
            return redirect()->route('admin/stores/update', ['id' => $store->id]);
        } else {
            // Redirecionamento
            return redirect()->route('admin/stores/update', ['id' => $store->id]);
        }
    }

    /**
     * Remove uma Loja
     */
    public function delete($id)
    {
        // Remoção
        Store::findOrFail($id)->delete();

        // Remover Imagem
        File::delete(base_path('storage/app/public/store-logo-' . $id));

        // Apresentação
        return redirect()->route('admin/stores');
    }
}
