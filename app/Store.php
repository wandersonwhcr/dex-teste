<?php

namespace App;

use Eloquent;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Mutable;

class Store extends Eloquent
{
    use Eloquence, Mappable, Mutable;

    protected $maps = [
        'openingHoursBegin' => 'opening_hours_begin',
        'openingHoursEnd'   => 'opening_hours_end',
    ];

    protected $getterMutators = [
        'openingHoursBegin' => 'App\Store@formatTime',
        'openingHoursEnd'   => 'App\Store@formatTime',
    ];

    /**
     * Efetua a Formatação de Campos de Hora
     *
     * @param  string|null $value Valor para Formatação
     * @return string|null Valor Resultante
     */
    public static function formatTime(string $value = null)
    {
        return isset($value) ? substr($value, 0, 5) : '';
    }
}
