# Lojas Abertas

Este projeto representa o teste solicitado para verificação do modelo de
programação utilizado pelos futuros colaboradores da dEx.

O ambiente de desenvolvimento utiliza o Vagrant para instalação de dependências.
Além disso, como gerenciamento de pacotes de _backend_ e _frontend_, utiliza o
Composer e o Bower.

Para tanto, precisamos inicializar o Vagrant para este projeto através do
comando abaixo.

```
vagrant up
```

Após que todas as dependências sejam instaladas na máquina virtual do Vagrant
através do Puppet, devemos, ainda, instalar as depedências do projeto utilizando
o comando a seguir.

```
vagrant ssh -c 'cd /vagrant && make'
```

## Explicando a Solução

Conforme solicitado, criou-se um pequeno gerenciamento de lojas e uma listagem
de lojas abertas no horário atual. Vale informar que a hora atual utilizada é a
configurada no servidor, usufruindo de seu fuso horário para consulta no banco
de dados.

A área do usuário é a página inicial do projeto, acessível através do endereço
`http://localhost:8000`. Neste local, são apresentadas informações sobre todas
as lojas cadastradas que estão abertas no horário atual.

O gerenciamento de lojas pode ser efetuado no endereço
`http://localhost:8000/admin/lojas`. Esta visualização apresenta todas as lojas
cadastradas no gerenciamento em ordem crescente de criação. Neste local, também
podemos acessar o formulário de criação de lojas, além de acessar o formulário
para edição e remoção de lojas. Basicamente, este cadastro funciona como um CRUD
simples.

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
