$(function () {
    // Editor para Descrição de Loja
    CKEDITOR.replace('store-description');
    // Captura de Horários de Loja
    $('#store-openingHoursBegin, #store-openingHoursEnd').timepicker({
        "showInputs": false,
        "showMeridian": false,
        "defaultTime": false
    });
});
