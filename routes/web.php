<?php

use App\Store;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Stores@list')->name('stores');
Route::get('/lojas/logotipo/{id}', 'Stores@logo')->name('stores/logo');

Route::get('/admin/lojas', 'AdminStores@list')->name('admin/stores');
Route::get('/admin/lojas/criar', 'AdminStores@form')->name('admin/stores/create');
Route::get('/admin/lojas/editar/{id}', 'AdminStores@form')->name('admin/stores/update');
Route::get('/admin/lojas/remover/{id}', 'AdminStores@remove')->name('admin/stores/delete');

Route::post('/admin/lojas/criar', 'AdminStores@post');
Route::post('/admin/lojas/editar/{id}', 'AdminStores@post');
Route::delete('/admin/lojas/remover/{id}', 'AdminStores@delete');
