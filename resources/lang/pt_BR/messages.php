<?php
return [
    'Actions'           => 'Ações',
    'Attribute'         => 'Atributo',
    'Begin Hour'        => 'Horário Inicial',
    'Create'            => 'Criar',
    'Description'       => 'Descrição',
    'Edit Store'        => 'Editar Loja',
    'Empty Stores'      => 'Nenhuma Loja Encontrada',
    'End Hour'          => 'Horário Final',
    'External'          => 'Externo',
    'General Info'      => 'Informações Básicas',
    'List Stores'       => 'Listar Lojas',
    'List'              => 'Listar',
    'Logo'              => 'Logotipo',
    'Main'              => 'Principal',
    'Name'              => 'Nome',
    'Opening Hours'     => 'Horário de Funcionamento',
    'Picture'           => 'Imagem',
    'Remove Store'      => 'Remover Loja',
    'Remove'            => 'Remover',
    'Save'              => 'Salvar',
    'Stores'            => 'Lojas',
    'Stores Open'       => 'Lojas Abertas',
    'Toggle Navigation' => 'Alternar Navegação',
    'Value'             => 'Valor',
];
