<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>dEx Teste | @lang('messages.Stores')</title>
        <link href="{{ asset('assets/external/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/external/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/external/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/external/adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/external/adminlte/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/stores.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="sidebar-mini skin-blue">
        <div class="wrapper">
            <header class="main-header">
                <a href="{{ route('stores') }}" class="logo">
                    <span class="logo-mini">dEx</span>
                    <span class="logo-lg">dEx Teste</span>
                </a><!-- logo -->
                <nav class="navbar navbar-static-top">
                    <a href="{{ route('stores') }}" class="sidebar-toggle" data-toggle="offcanvas">
                        <span class="sr-only">@lang('messages.Toggle Navigation')</span>
                    </a><!-- sidebar-toggle -->
                </nav><!-- navbar -->
            </header><!-- main-header -->
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="nav sidebar-menu">
                        <li class="header">@lang('messages.Main')</li>
                        <li class="active"><a href="{{ route('admin/stores') }}"><i class="fa fa-cube fa-fw"></i> @lang('messages.Stores')</a><li>
                        <li class="header">@lang('messages.External')</li>
                        <li><a href="{{ route('stores') }}"><i class="fa fa-cube fa-fw"></i> @lang('messages.Stores Open')</a></li>
                    </ul><!-- sidebar-menu -->
                </section><!-- sidebar -->
            </aside><!-- main-sidebar -->
            <div class="content-wrapper">
                <div class="content-header">
                    <h1>@lang('messages.Stores') <small>@lang('messages.List')</small></h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ route('admin/stores') }}"><i class="fa fa-cube fa-fw"></i> @lang('messages.Stores')</a></li>
                        <li class="active">@lang('messages.List')</li>
                    </ol><!-- breadcrumb -->
                </div><!-- content-header -->
                <div class="content">
                    @include('common/errors')
                    @yield('content')
                </div><!-- content -->
            </div><!-- content-wrapper -->
            <footer class="main-footer">
            </footer><!-- main-footer -->
        </div><!-- wrapper -->
        <script type="text/javascript" src="{{ asset('assets/external/jquery/dist/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/external/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/external/adminlte/dist/js/app.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/external/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/external/ckeditor/ckeditor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/stores.js') }}"></script>
    </body>
</html>
