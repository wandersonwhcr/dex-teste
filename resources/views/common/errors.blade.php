@if (count($errors) > 0)
    <div class="alert-container">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible">
                <button class="close" type="button" data-dismiss="alert">&times;</button>
                {{ $error }}
            </div><!-- alert -->
        @endforeach
    </div><!-- alert-container -->
@endif
