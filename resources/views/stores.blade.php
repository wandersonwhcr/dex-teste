<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>dEx Teste - Lojas</title>
        <link href="{{ asset('assets/external/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/external/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/stores.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>@lang('messages.Stores Open')</h1>
            </div><!-- page-header -->
            @if (count($stores) > 0)
                <div class="row">
                    @foreach ($stores as $store)
                        <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">
                                        <i class="fa fa-cube fa-fw"></i>
                                        {{ $store->name }}
                                        <small>{{ $store->openingHoursBegin }}-{{ $store->openingHoursEnd }}</small>
                                    </h2>
                                </div><!-- panel-heading -->
                                <div class="panel-body">
                                    {!! $store->description !!}
                                </div><!-- panel-body -->
                                <div class="panel-footer">
                                    <img src="{{ route('stores/logo', ['id' => $store->id]) }}" class="img-responsive" alt="Logotipo">
                                </div><!-- panel-footer -->
                            </div><!-- panel -->
                        </div><!-- col -->
                    @endforeach
                </div><!-- row -->
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            @lang('messages.Empty Stores')
                        </div><!-- alert -->
                    </div><!-- col -->
                </div><!-- row -->
            @endif
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="{{ route('stores') }}">@lang('messages.Stores Open')</a></li>
                        <li><a href="{{ route('admin/stores') }}">@lang('messages.List Stores')</a></li>
                    </ul>
                </div><!-- col -->
            </div><!-- row -->
        </div><!-- container -->
        <script type="text/javascript" src="{{ asset('assets/external/jquery/dist/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/external/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/stores.js') }}"></script>
    </body>
</html>
