@extends('layouts.admin')
@section('content')

<form method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-app">
                <i class="fa fa-save"></i> @lang('messages.Save')
            </button>
        </div><!-- col -->
    </div><!-- row -->

    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <div class="box-title">
                        <i class="fa fa-file fa-fw"></i> @lang('messages.General Info')
                    </div>
                </div><!-- box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="store-name" class="control-label">@lang('messages.Name')</label>
                                <input id="store-name" type="text" name="store[name]" class="form-control" value="{{ old('store.name', $store->name) }}">
                            </div><!-- form-group -->
                        </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="store-description" class="control-label">@lang('messages.Description')</label>
                                <textarea id="store-description" type="text" name="store[description]" class="form-control">{{ old('store.description', $store->description) }}</textarea>
                            </div><!-- form-group -->
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- box-body -->
            </div><!-- box -->
        </div><!-- col -->
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="box-title">
                                <i class="fa fa-clock-o fa-fw"></i> @lang('messages.Opening Hours')
                            </div><!-- box-title -->
                        </div><!-- box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group bootstrap-timepicker">
                                        <label for="store-openingHoursBegin" class="control-label">@lang('messages.Begin Hour')</label>
                                        <input id="store-openingHoursBegin" type="text" name="store[openingHoursBegin]" class="form-control" value="{{ old('store.openingHoursBegin', $store->openingHoursBegin) }}">
                                    </div><!-- form-group -->
                                </div><!-- col -->
                            </div><!-- row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group bootstrap-timepicker">
                                        <label for="store-openingHoursEnd" class="control-label">@lang('messages.End Hour')</label>
                                        <input id="store-openingHoursEnd" type="text" name="store[openingHoursEnd]" class="form-control" value="{{ old('store.openingHoursEnd', $store->openingHoursEnd) }}">
                                    </div><!-- form-group -->
                                </div><!-- col -->
                            </div><!-- row -->
                        </div><!-- box-body -->
                    </div><!-- box -->
                </div><!-- col -->
            </div><!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="box-title">
                                <i class="fa fa-picture-o"></i> @lang('messages.Logo')
                            </div><!-- box-title -->
                        </div><!-- box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="store-logo" class="control-label">@lang('messages.Picture')</label>
                                        <input id="store-logo" type="file" name="store[logo]">
                                    </div><!-- form-group -->
                                </div><!-- col -->
                            </div><!-- row -->
                        </div><!-- box-body -->
                    </div><!-- box -->
                </div><!-- col -->
            </div><!-- row -->
        </div><!-- col -->
    </div><!-- row -->
</form>

@endsection
