@extends('layouts.admin')
@section('content')

<form method="POST">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-app">
                <i class="fa fa-times"></i> @lang('messages.Remove')
            </button>
        </div><!-- col -->
    </div><!-- row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-title">
                        <i class="fa fa-file fa-fw"></i> @lang('messages.General Info')
                    </div><!-- box-title -->
                </div><!-- box-header -->
                <div class="box-body no-padding table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="col-xs-4">@lang('messages.Attribute')</th>
                                <th class="col-xs-8">@lang('messages.Value')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>@lang('messages.Name')</th>
                                <td>{{ $store->name }}</td>
                            </tr>
                            <tr>
                                <th>@lang('messages.Description')</th>
                                <td>{!! $store->description !!}</td>
                            </tr>
                            <tr>
                                <th>@lang('messages.Opening Hours')</th>
                                <td>{{ $store->openingHoursBegin }} - {{ $store->openingHoursEnd }}</td>
                            </tr>
                            <tr>
                                <th>@lang('messages.Logo')</th>
                                <td><img src="{{ route('stores/logo', ['id' => $store->id]) }}" alt="Logotipo"></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- box-body -->
            </div><!-- box -->
        </div><!-- col -->
    </div><!-- row -->
</form>

@endsection
