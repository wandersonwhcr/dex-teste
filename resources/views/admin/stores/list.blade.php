@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-app" href="{{ route('admin/stores/create') }}">
            <i class="fa fa-plus"></i> @lang('messages.Create')
        </a><!-- btn-app -->
    </div><!-- col -->
</div><!-- row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <div class="box-title">
                    <i class="fa fa-cube fa-fw"></i> @lang('messages.Stores')
                </div>
            </div><!-- box-header -->
            <div class="box-body no-padding table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-xs-11">@lang('messages.Name')</th>
                            <th class="col-xs-1">@lang('messages.Actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($stores) > 0)
                            @foreach ($stores as $store)
                                <tr>
                                    <td>{{ $store->name }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-default" href="{{ route('admin/stores/update', ['id' => $store->id]) }}" title="@lang('messages.Edit Store')">
                                                <i class="fa fa-pencil fa-fw"></i>
                                            </a>
                                            <a class="btn btn-danger" href="{{ route('admin/stores/delete', ['id' => $store->id]) }}" title="@lang('messages.Remove Store')">
                                                <i class="fa fa-times fa-fw"></i>
                                            </a>
                                        </div><!-- btn-group -->
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="text-center">@lang('messages.Empty Stores')</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div><!-- box-body -->
        </div><!-- box -->
    </div><!-- col -->
</div><!-- row -->

@endsection
