Exec {
    path    => ["/usr/bin", "/bin", "/usr/sbin", "/sbin" ],
    timeout => 0,
}

Package {
    require => Exec["apt-get : update"],
    before  => Exec["apt-get : autoremove"],
}

exec { "apt-get : update":
    command => "apt-get update",
}

exec { "apt-get : autoremove":
    command => "apt-get autoremove -q -y",
}

exec { "apt-get : https : update":
    command => "apt-get update",
}

package { "apt-get : https":
    name    => "apt-transport-https",
    require => Exec["apt-get : https : update"],
}

package { "vim":
    name => "vim",
}

package { "git":
    name => "git",
}

package { "unzip":
    name => "unzip",
}

package { "exim4":
    name   => "exim4-base",
    ensure => "purged",
}

package { "nano":
    name   => "nano",
    ensure => "purged",
}

package { "rpcbind":
    name   => "rpcbind",
    ensure => "purged",
}

exec { "nginx : key":
    unless  => "apt-key list | grep nginx",
    command => "curl http://nginx.org/keys/nginx_signing.key | apt-key add -",
}

file { "nginx : list":
    path    => "/etc/apt/sources.list.d/nginx.list",
    content => "deb http://nginx.org/packages/debian/ jessie nginx",
    require => Exec["nginx : key"],
    before  => Exec["apt-get : update"],
}

package { "nginx":
    name    => "nginx",
    require => [
        File["nginx : list"],
        Exec["apt-get : update"],
    ],
}

user { "vagrant":
    groups  => ["vagrant", "www-data"],
    require => Package["nginx"],
}

service { "nginx":
    ensure  => "running",
    enable  => true,
    require => Package["nginx"],
}

file { "nginx : conf":
    path    => "/etc/nginx/nginx.conf",
    source  => "puppet:///modules/archive/nginx_conf",
    require => Package["nginx"],
    notify  => Service["nginx"],
}

file { "nginx : default":
    ensure  => absent,
    path    => "/etc/nginx/conf.d/default.conf",
    require => Package["nginx"],
    notify  => Service["nginx"],
}

file { "nginx : virtualhost":
    path    => "/etc/nginx/conf.d/10-default.conf",
    source  => "puppet:///modules/archive/nginx_virtualhost",
    require => Package["nginx"],
    notify  => Service["nginx"],
}

file { "nginx : fastcgi":
    path    => "/etc/nginx/fastcgi_params",
    source  => "puppet:///modules/archive/nginx_fastcgi",
    require => Package["nginx"],
    notify  => Service["nginx"],
}

exec { "php : key":
    unless  => "apt-key list | grep dotdeb",
    command => "curl http://www.dotdeb.org/dotdeb.gpg | apt-key add -",
}

file { "php : list":
    path    => "/etc/apt/sources.list.d/php.list",
    content => "deb http://packages.dotdeb.org jessie all",
    require => Exec["php : key"],
    before  => Exec["apt-get : update"],
}

package { "php : cli":
    name    => "php-cli",
    require => [
        File["php : list"],
        Exec["apt-get : update"],
    ],
}

package { "php : fpm":
    name    => "php-fpm",
    require => [
        File["php : list"],
        Exec["apt-get : update"],
    ],
}

service { "php":
    name    => "php7.0-fpm",
    ensure  => "running",
    enable  => true,
    require => Package["php : fpm"],
}

package { "php : mbstring":
    name    => "php-mbstring",
    notify  => Service["php"],
    require => [
        File["php : list"],
        Exec["apt-get : update"],
    ],
}

package { "php : xml":
    name    => "php-xml",
    notify  => Service["php"],
    require => [
        File["php : list"],
        Exec["apt-get : update"],
    ],
}

package { "php : mysql":
    name    => "php-mysql",
    notify  => [
        Service["php"],
    ],
    require => [
        File["php : list"],
        Exec["apt-get : update"],
    ],
}

package { "mysql : server":
    name => "mysql-server"
}

exec { "mysql : password":
    command => "mysqladmin -u root password 'root'",
    unless  => "mysql -u root -proot -e 'SELECT CURDATE()' > /dev/null",
    require => [
        Package["mysql : server"],
    ],
}

exec { "composer":
    creates => "/usr/bin/composer",
    command => "curl https://getcomposer.org/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer",
}

exec { "composer : update":
    command     => "composer self-update",
    environment => "COMPOSER_HOME=/root/.composer",
    require     => [
        Package["php : cli"],
        Exec["composer"],
    ],
}

exec { "node : key":
    unless  => "apt-key list | grep nodesource",
    command => "curl https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -",
}

file { "node : list":
    path    => "/etc/apt/sources.list.d/node.list",
    content => "deb https://deb.nodesource.com/node_6.x jessie main",
    before  => Exec["apt-get : update"],
    require => [
        Exec["node : key"],
        Package["apt-get : https"],
    ],
}

package { "node":
    name => "nodejs",
}

exec { "bower":
    creates => "/usr/bin/bower",
    command => "npm install -g bower",
    require => Package["node"],
}

exec { "default : database":
    command => "mysql -u root -proot -e 'CREATE DATABASE vagrant'",
    unless  => "mysql -u root -proot -e 'USE vagrant'",
    require => [
        Exec["mysql : password"],
    ],
}
